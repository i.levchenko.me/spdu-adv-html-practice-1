import {displayCharactersData} from './display-characters-data';
import {displayCharacterData} from './display-charater-data';

const ROOT = document.getElementById('root');
const FORM = document.getElementById('form');
const LOAD_MORE_BUTTON = document.getElementById('#load-more-button');
const isHomePage = window.location.pathname === '/';
const isSinglePage = window.location.pathname.match(/\/\d{7}/);

const {loadCharacters, setCharactersQueries, resetPage} = displayCharactersData(ROOT);
const {loadCharacter} = displayCharacterData(ROOT);

if (isHomePage) {
  loadCharacters();
}

if (isSinglePage) {
  loadCharacter(window.location.pathname.replace(/^\//, ''));
}

LOAD_MORE_BUTTON && (LOAD_MORE_BUTTON.addEventListener('click', () => {
  if (isHomePage) {
    loadCharacters();
  }
}));

FORM.addEventListener('submit', event => {
  window.history.pushState({}, '', window.location.origin);
  new FormData(FORM);
});

FORM.addEventListener('formdata', event => {
    setCharactersQueries(Object.fromEntries(event.formData));
    resetPage();
    loadCharacters();
});
